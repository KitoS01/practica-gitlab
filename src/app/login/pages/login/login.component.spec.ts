/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { LoginComponent } from './login.component';

import { LoginService } from '../../services/login.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { of } from 'rxjs';

class LoginServiceStub {
  validarUsuario(value: any) {
    // Utiliza value para determinar el comportamiento basado en el estado de autenticación
    if (value === 'caso_exitoso') {
      return of({ status: 200, token: 'fakeToken', message: 'Inicio de sesión correcto' });
    } else {
      return of({ status: 400, token: 'fakeToken', message: 'Error al iniciar sesión' });
    }
  }
  }
  
  class RouterStub {
    navigateByUrl(url: string) { return url; }
  }

  describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
  TestBed.configureTestingModule({
    declarations: [ LoginComponent ],
    providers: [
      FormBuilder,
      { provide: LoginService, useClass: LoginServiceStub },
      { provide: Router, useClass: RouterStub }
    ]
  })
  .compileComponents();
}));


  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should authenticate user when form is valid and navigate to inicio', () => {
  spyOn(component.getLoginService(), 'validarUsuario').and.returnValue(of({ status: 200, token: 'fakeToken', message: 'Inicio de sesión correcto' }));
  spyOn(component, 'navigateToInicio'); 

  component.myForm.setValue({ email: 'kireluriel@gmail.com', password: 'Vicente000-' });
  component.auth();

  expect(component.getLoginService().validarUsuario).toHaveBeenCalled();
  expect(component.navigateToInicio).toHaveBeenCalled();
});

it('should display an alert when password is incorrect', () => {
  spyOn(window, 'alert');

  it('should display an alert when password is incorrect', () => {
    spyOn(component.getLoginService(), 'validarUsuario').and.returnValue(of({ status: 400, token: 'fakeToken', message: 'Error al iniciar sesión' }));

  component.myForm.setValue({ email: 'kireluriel@gmail.com', password: 'Vicente00-' });
  component.auth();

  expect(window.alert).toHaveBeenCalledWith('Contraseña incorrecta');
});

});
})
