export interface RespuestaLogin {
    message: string;
    status: number;
    token: string; // Cambiado de number a string
}
