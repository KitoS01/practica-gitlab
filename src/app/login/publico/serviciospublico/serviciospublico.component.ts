import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-serviciospublico',
  templateUrl: './serviciospublico.component.html',
  styleUrls: ['./serviciospublico.component.css']
})
export class ServiciospublicoComponent implements OnInit {
  servicio1: string="assets/images/servicio1.jpg";
  servicio2: string="assets/images/servicio2.jpg";
  servicio3: string="assets/images/servicio3.jpg";
  servicio4: string="assets/images/servicio4.jpg";
  servicio5: string="assets/images/servicio5.jpg";
  servicio6: string="assets/images/servicio6.jpg";
  brackets: string="assets/images/brackets.jpg";
  implante: string="assets/images/implante.jpg";
  valoracion: string="assets/images/valoracion.jpg";
  extraccion: string="assets/images/extraccion.jpg";
  limpieza: string="assets/images/limpieza.jpg";

  imagenSeleccionada: boolean = false;

  
  seleccionarImagen() {
    this.imagenSeleccionada = true;
  }
  
  deseleccionarImagen() {
    this.imagenSeleccionada = false;
  }
  
  constructor() { }

  ngOnInit() {
  }

}
